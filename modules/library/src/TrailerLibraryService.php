<?php

namespace Drupal\unsm_trailer_library;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\unsm_trailer_library\Client\UnsinnLibraryClientInterface;
use GuzzleHttp\ClientInterface;

/**
 * Default trailer library service implementation.
 */
class TrailerLibraryService implements TrailerLibraryServiceInterface {

  /**
   * The file storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Guzzle HTTP Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The UNSINN trailer library client.
   *
   * @var \Drupal\unsm_trailer_library\Client\UnsinnLibraryClientInterface
   */
  protected $libraryClient;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The trailer storage.
   *
   * @var \Drupal\trailer\TrailerStorageInterface
   */
  protected $trailerStorage;

  /**
   * Constructs a new TrailerLibraryService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\unsm_trailer_library\Client\UnsinnLibraryClientInterface $library_client
   *   The UNSINN trailer library client.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, ClientInterface $http_client, UnsinnLibraryClientInterface $library_client) {
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
    $this->libraryClient = $library_client;
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
    $this->trailerStorage = $entity_type_manager->getStorage('trailer');
  }

  /**
   * {@inheritdoc}
   */
  public function cloneFromLibrary($remote_id, $save = TRUE) {
    $remote_trailer = $this->libraryClient->getTrailerDetails($remote_id);
    if (empty($remote_trailer)) {
      return NULL;
    }

    /** @var \Drupal\trailer\Entity\TrailerInterface $trailer */
    $trailer = $this->trailerStorage->create([
      'remote_id' => $remote_trailer->getId(),
      'type' => $remote_trailer->getBundle(),
      'status' => $remote_trailer->isPublished(),
      'title' => $remote_trailer->getTitle(),
    ]);
    if ($internal_dimensions = $remote_trailer->getInternalDimensions()) {
      $trailer->set('internal_dimensions', $internal_dimensions);
    }
    if ($loading_height = $remote_trailer->getLoadingHeight()) {
      $trailer->setLoadingHeight($loading_height);
    }
    if ($inner_height = $remote_trailer->getInnerHeight()) {
      $trailer->set('inner_height', $inner_height->toArray());
    }
    if ($load_capacity = $remote_trailer->getLoadCapacity()) {
      $trailer->setLoadCapacity($load_capacity);
    }
    if ($total_weight = $remote_trailer->getTotalWeight()) {
      $trailer->setTotalWeight($total_weight);
    }
    if ($description = $remote_trailer->getDescription()) {
      $trailer->set('description', [
        'value' => $description,
        'format' => 'basic_html',
      ]);
    }
    if ($price = $remote_trailer->getPrice()) {
      $trailer->setPrice($price);
    }

    if ($remote_image_url = $remote_trailer->getImageUrl()) {
      $directory = 'public://unsinn-anhaenger';
      if ($remote_trailer->getBundle() == 'remaining_stock') {
        $directory .= '/aktionsanhaenger';
      }
      elseif ($remote_trailer->getBundle() == 'secondhand') {
        $directory .= '/gebrauchtanhaenger';
      }
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
      $filename = $this->fileSystem->basename($remote_image_url);
      $destination = sprintf('%s/%s', $directory, $filename);
      $final_destination = $this->fileSystem->getDestinationFilename($destination, FileSystemInterface::EXISTS_RENAME);
      $destination_stream = @fopen($final_destination, 'w');
      if ($destination_stream === FALSE) {
        return NULL;
      }
      $this->httpClient->get($remote_image_url, ['sink' => $destination_stream]);
      $file = $this->fileStorage->create([
        'uri' => $final_destination,
      ]);
      $file->setFilename($this->fileSystem->basename($final_destination));
      $file->save();

      $trailer->set('field_images', $file->id());
    }

    if ($category_name = $remote_trailer->getCategoryName()) {
      /** @var \Drupal\taxonomy\TermInterface[] $categories */
      $categories = $this->termStorage->loadByProperties([
        'vid' => 'trailer_categories',
        'name' => $category_name,
      ]);
      if (!empty($categories)) {
        $category = reset($categories);
        $trailer->setCategory($category);
      }
      else {
        /** @var \Drupal\taxonomy\TermInterface $category */
        $category = $this->termStorage->create([
          'vid' => 'trailer_categories',
          'name' => $category_name,
        ]);
        $category->save();
        $trailer->setCategory($category);
      }
    }

    if ($save) {
      $trailer->save();
    }

    return $trailer;
  }

}
