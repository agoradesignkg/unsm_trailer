<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info_alter().
 */
function unsm_trailer_token_info_alter(&$data) {
  if (!empty($data['tokens']['webform_submission'])) {
    $data['tokens']['webform_submission']['enquiry-discounted-rendered'] = [
      'name' => t('Rendered enquiry (discounted trailers)'),
      'description' => t('Special token for "enquiry discounted" webform submissions: HTML rendering of the submission including listing of selected trailer.'),
    ];
  }
}

/**
 * Implements hook_tokens().
 */
function unsm_trailer_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
//  $token_service = \Drupal::token();

  // Set URL options to generate absolute translated URLs.
  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }

  $replacements = [];
  if ($type == 'webform_submission' && !empty($data['webform_submission'])) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $data['webform_submission'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'enquiry-discounted-rendered':
          $build = [
            '#theme' => 'enquiry_discounted_webform_submission',
            '#webform_submission' => $webform_submission,
            '#include_personal_data' => TRUE,
            '#include_address_data' => TRUE,
            '#include_message' => TRUE,
            '#include_selected_trailer' => TRUE,
          ];
          // included in an email.
          $replacements[$original] = \Drupal::service('renderer')->renderPlain($build);
          break;
      }
    }
  }
  return $replacements;
}
