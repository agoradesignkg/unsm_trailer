<?php

namespace Drupal\unsm_trailer_library\Client;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\unsm_trailer_library\LibraryTrailer;
use Drupal\unsm_trailer_library\TrailerLibraryList;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * Default UNSINN trailer library client implementation.
 */
class UnsinnLibraryClient implements UnsinnLibraryClientInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The trailer library endpoint url.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * The trailer library details endpoint url.
   *
   * @var string
   */
  protected $detailsEndpoint;

  /**
   * The trailer library client name.
   *
   * @var string
   */
  protected $clientName;

  /**
   * The trailer library access token.
   *
   * @var string
   */
  protected $token;

  /**
   * Static cache. Handy for having access from multiple places to the result.
   *
   * Without the need of querying over and over again.
   *
   * @var array
   */
  protected $responseCache;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Encoder\DecoderInterface
   */
  protected $serializer;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new UnsinnLibraryClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Symfony\Component\Serializer\Encoder\DecoderInterface $serializer
   *   The serializer.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, DecoderInterface $serializer, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->serializer = $serializer;
    $this->logger = $logger;
    $config = $config_factory->get('unsm_trailer_library.settings');
    $this->endpoint = $config->get('endpoint');
    $this->detailsEndpoint = $config->get('details_endpoint');
    $this->clientName = $config->get('client_name');
    $this->token = $config->get('token');
    $this->responseCache = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTrailerList() {
    $cache_key = 'list';
    if (isset($this->responseCache[$cache_key])) {
      // Return the cached response.
      return $this->responseCache[$cache_key];
    }

    $target_url = Url::fromUri($this->endpoint);
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $this->httpClient->get($target_url->toString(), [
      'headers' => [
        'Accept' => 'application/json',
        'UNSM-Client' => $this->clientName,
        'UNSM-Token' => $this->token,
      ],
    ]);

    $response_body = $this->serializer->decode($response->getBody(), 'json');
    if (empty($response_body) || !is_array($response_body)) {
      // @todo is an empty array the way to go for invalid responses?
      $this->responseCache[$cache_key] = [];
      return [];
    }
    $result = TrailerLibraryList::fromArray($response_body);
    $this->responseCache[$cache_key] = $result;
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrailerDetails($remote_id) {
    $response_body = $this->queryTrailerDetails($remote_id);
    if (empty($response_body)) {
      return NULL;
    }
    return LibraryTrailer::fromArray($response_body);
  }

  /**
   * Queries the trailer details web service, using static response cache.
   *
   * @param int $trailer_id
   *   The UNSINN trailer ID.
   *
   * @return array
   *   The received response as JSON array.
   */
  protected function queryTrailerDetails($trailer_id) {
    $cache_key = 'details:' . $trailer_id;
    if (isset($this->responseCache[$cache_key])) {
      // Return the cached response.
      return $this->responseCache[$cache_key];
    }

    $target_url = Url::fromUri(sprintf('%s/%s', $this->detailsEndpoint, $trailer_id));
    /** @var \GuzzleHttp\Psr7\Response $response */
    $response = $this->httpClient->get($target_url->toString(), [
      'headers' => [
        'Accept' => 'application/json',
        'UNSM-Client' => $this->clientName,
        'UNSM-Token' => $this->token,
      ],
    ]);

    $response_body = $this->serializer->decode($response->getBody(), 'json');
    if (empty($response_body) || !is_array($response_body)) {
      // @todo is an empty array the way to go for invalid responses?
      $this->responseCache[$cache_key] = [];
      return [];
    }
    $this->responseCache[$cache_key] = $response_body;
    return $response_body;
  }

}
