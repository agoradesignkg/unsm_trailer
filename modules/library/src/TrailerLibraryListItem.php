<?php

namespace Drupal\unsm_trailer_library;

use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;

/**
 * Value object representing a single trailer library item (for overview).
 */
final class TrailerLibraryListItem extends LibraryTrailerBase implements \Serializable {

  /**
   * {@inheritdoc}
   */
  public function serialize() {
    return Json::encode($this->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($serialized) {
    $json = Json::decode($serialized);
    $this->id = $json['id'];
    $this->title = $json['title'];
    $this->categoryName = $json['category'];
    $this->categoryId = $json['category_id'];

    $this->bundle = $json['bundle'];
    switch ($json['bundle']) {
      case 'remaining_stock':
        $this->bundleLabel = 'Aktionsanhänger';
        break;

      case 'secondhand':
        $this->bundleLabel = 'Gebrauchtanhänger';
        break;

      default:
        $this->bundleLabel = $json['bundle'];
    }
    if (!empty($json['price'])) {
      $this->price = new Price($json['price']['number'], $json['price']['currency_code']);
    }
    $this->status = (bool) $json['status'];
    $this->created = (int) $json['created'];
    $this->changed = (int) $json['changed'];
    return $this;
  }

}
