<?php

namespace Drupal\unsm_trailer\Webform;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\webform\WebformSubmissionInterface;
use Psr\Log\LoggerInterface;

/**
 * Default enquiry webform render helper implementation.
 */
class EnquiryWebformRenderHelper implements EnquiryWebformRenderHelperInterface {

  use NormalizeCountryTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new EnquiryWebformRenderHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersonalData(WebformSubmissionInterface $enquiry) {
    // Should we translate them, or fetch from the underlying webform entity?
    $webform_fields = [
      'salutation' => 'Anrede',
      'title' => 'Titel',
      'first_name' => 'Vorname',
      'last_name' => 'Nachname',
      'company' => 'Firma',
    ];
    return $this->getData($enquiry, $webform_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function getAddressData(WebformSubmissionInterface $enquiry) {
    // Should we translate them, or fetch from the underlying webform entity?
    $webform_fields = [
      'street' => 'Straße',
      'concat:postal_code:locality' => 'PLZ / Ort',
      'country' => 'Land',
      'phone' => 'Telefon',
      'email' => 'E-Mail',
    ];
    return $this->getData($enquiry, $webform_fields);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedTrailer(WebformSubmissionInterface $enquiry) {
    $trailer_id = intval($enquiry->getElementData('trailer'));
    if (empty($trailer_id) || $trailer_id < 1) {
      return NULL;
    }
    return $this->entityTypeManager->getStorage('trailer')->load($trailer_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedTrailerRenderable(WebformSubmissionInterface $enquiry) {
    $trailer = $this->getSelectedTrailer($enquiry);
    if (empty($trailer)) {
      return [];
    }
    return $this->entityTypeManager->getViewBuilder('trailer')->view($trailer, 'email');
  }

  /**
   * Helper function to retrieve values from the given webform submission.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $enquiry
   *   The webform submission entity.
   * @param string[] $webform_fields
   *   An array of webform fields, keyed by field name, having the labels as
   *   value. Special concatenation keys are supported: if the key starts with
   *   'concat:', then multiple key names may follow, separated by ':'.
   *
   * @return string[]
   *   The submitted data as array, having the labels as key and the data as
   *   values.
   */
  protected function getData(WebformSubmissionInterface $enquiry, array $webform_fields) {
    $data = [];
    foreach ($webform_fields as $field => $label) {
      if (substr($field, 0, 7) == 'concat:') {
        $field_names = explode(':', $field);
        array_shift($field_names);
        $values = [];
        foreach ($field_names as $field_name) {
          $single_value = $enquiry->getElementData($field_name);
          if (!empty($single_value)) {
            $values[] = $single_value;
          }
        }
        $value = implode(' ', $values);
      }
      else {
        $value = $enquiry->getElementData($field);
      }

      if (!empty($value)) {
        $data[$label] = $value;
      }
    }
    return $data;
  }

}
