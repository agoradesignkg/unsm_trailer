<?php

namespace Drupal\unsm_trailer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\trailer\Entity\TrailerInterface;

/**
 * Defines the enquiry controller.
 */
class EnquiryController extends ControllerBase {

  /**
   * Shows the enquiry form for the given trailer.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   *
   * @return array
   *   The response as render array.
   */
  public function form(TrailerInterface $trailer) {
    $page = [
      '#theme' => 'enquiry_page',
    ];
    $page['#trailer_pre_header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => 'Ihr gewählter Anhänger',
      '#weight' => 0,
    ];
    $page['#trailer'] = $this->entityTypeManager()->getViewBuilder('trailer')->view($trailer, 'summary');
    $page['#trailer']['#weight'] = 5;

    $page['#accessories_pre_header'] = [];
    $page['#accessories'] = [];

    $page['#enquiry'] = [
      '#type' => 'webform',
      '#webform' => 'enquiry_discounted',
      '#default_data' => [
        'trailer' => $trailer->id(),
      ],
      '#weight' => 20,
    ];

    return $page;
  }

}
