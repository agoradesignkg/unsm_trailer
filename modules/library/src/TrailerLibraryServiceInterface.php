<?php

namespace Drupal\unsm_trailer_library;

/**
 * Defines the trailer library service interface.
 */
interface TrailerLibraryServiceInterface {

  /**
   * Clones the given remote trailer (by ID) to a local trailer entity.
   *
   * @param int $remote_id
   *   The remote trailer ID.
   * @param bool $save
   *   Whether to save the entity. Defaults to TRUE.
   *
   * @return \Drupal\trailer\Entity\TrailerInterface|null
   *   The newly created trailer entity, or NULL, if the given remote is invalid
   *   or another error occurred.
   */
  public function cloneFromLibrary($remote_id, $save = TRUE);

}
