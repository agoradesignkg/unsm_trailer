<?php

namespace Drupal\unsm_trailer_library\Client;

/**
 * Defines the UNSINN trailer library client interface.
 */
interface UnsinnLibraryClientInterface {

  /**
   * Get the trailer list from unsinn.de
   *
   * @return \Drupal\unsm_trailer_library\TrailerLibraryList
   *   A list of all available trailers from unsinn.de library.
   *
   * @throws \GuzzleHttp\Exception\ClientException
   *   E.g. on access forbidden due to missing or invalid credentials.
   */
  public function getTrailerList();

  /**
   * Gets trailer details for the given ID.
   *
   * @param int $remote_id
   *   UNSINN trailer ID.
   *
   * @return \Drupal\unsm_trailer_library\LibraryTrailer|null
   */
  public function getTrailerDetails($remote_id);

}
