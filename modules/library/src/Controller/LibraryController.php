<?php

namespace Drupal\unsm_trailer_library\Controller;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Url;
use Drupal\unsm_trailer_library\Client\UnsinnLibraryClientInterface;
use Drupal\unsm_trailer_library\TrailerLibraryListItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * The trailer library controller.
 */
class LibraryController extends ControllerBase {

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The key value store to use.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * The UNSINN trailer library client.
   *
   * @var \Drupal\unsm_trailer_library\Client\UnsinnLibraryClientInterface
   */
  protected $libraryClient;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new LibraryController object.
   *
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   *   The currency formatter.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   * @param \Drupal\unsm_trailer_library\Client\UnsinnLibraryClientInterface $library_client
   *   The UNSINN trailer library client.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(CurrencyFormatterInterface $currency_formatter, DateFormatterInterface $date_formatter, KeyValueFactoryInterface $key_value_factory, UnsinnLibraryClientInterface $library_client, TimeInterface $time) {
    $this->currencyFormatter = $currency_formatter;
    $this->dateFormatter = $date_formatter;
    $this->keyValueStore = $key_value_factory->get('unsm_trailer_library');
    $this->libraryClient = $library_client;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_price.currency_formatter'),
      $container->get('date.formatter'),
      $container->get('keyvalue'),
      $container->get('unsm_trailer_library.api_client'),
      $container->get('datetime.time')
    );
  }

  /**
   * Renders the overview page.
   *
   * @return array
   *   A render array.
   */
  public function overview() {
    $build = [];
    $last_refresh = $this->keyValueStore->get('last_refresh', 0);
    /** @var \Drupal\unsm_trailer_library\TrailerLibraryListItem[] $items */
    $items = $this->keyValueStore->get('trailers', []);

    $build['header'] = [
      '#type' => 'inline_template',
      '#template' => '<h4>Letzte Synchronisierung: {{ last_sync }} <a href="{{ sync_url }}">Jetzt aktualisieren</a></h4>',
      '#context' => [
        'last_sync' => $last_refresh ? $this->dateFormatter->format($last_refresh, 'short') : 'nie',
        'sync_url' => Url::fromRoute('unsm_trailer_library.trailer_library.sync'),
      ],
    ];

    // @todo translations.
    $table_header = [
      'title' => 'Bezeichnung',
      'category' => 'Typ',
      'bundle' => 'Art',
      'price' => 'Preis',
      'status' => 'Veröffentlicht',
      'created' => 'Erstellungsdatum',
      'changed' => 'Zuletzt geändert',
      'operations' => $this->t('Operations'),
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => $table_header,
      '#title' => 'UNSINN Anhängerbibliothek',
      '#rows' => [],
      '#empty' => 'Keine Anhänger verfügbar. Überprüfen Sie die Anmeldedaten!',
    ];

    $used_trailer_ids = unsm_trailer_library_get_used_remote_trailer_ids();
    foreach ($items as $item) {
      $build['table']['#rows'][$item->getId()] = [
        'title' => $item->getTitle(),
        'category' => $item->getCategoryName(),
        'bundle' => $item->getBundleLabel(),
        'price' => $item->getPrice() ? $this->currencyFormatter->format($item->getPrice()->getNumber(), $item->getPrice()->getCurrencyCode()) : '',
        'status' => $item->isPublished() ? $this->t('Yes') : $this->t('No'),
        'created' => $this->dateFormatter->format($item->getCreated(), 'short'),
        'changed' => $this->dateFormatter->format($item->getChanged(), 'short'),
        'operations' => ['data' => $this->buildOperations($item, isset($used_trailer_ids[$item->getId()]))],
      ];
    }

    return $build;
  }

  /**
   * Syncs the trailer library items with unsinn.de webservice.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirection to overview page, displaying a message about success.
   */
  public function sync() {
    try {
      $trailer_list = $this->libraryClient->getTrailerList();
      $this->keyValueStore->set('trailers', $trailer_list->getItems());
      $this->keyValueStore->set('last_refresh', $this->time->getRequestTime());
      $this->messenger()->addStatus($this->t('The trailer library has been updated!'));
    }
    catch (\Exception $ex) {
      $this->messenger()->addWarning($ex->getMessage());
    }
    return new RedirectResponse(Url::fromRoute('unsm_trailer_library.trailer_library.overview')->toString());
  }

  /**
   * Builds a renderable list of operation links for the item.
   *
   * @param \Drupal\unsm_trailer_library\TrailerLibraryListItem $item
   *   The trailer library item on which the linked operations will be performed.
   * @param bool $is_used
   *   Whether the item is already used in a trailer entity on this website.
   *
   * @return array
   *   A renderable array of operation links.
   */
  protected function buildOperations(TrailerLibraryListItem $item, $is_used) {
    $operations = [];
    if ($is_used) {
      $trailers = unsm_trailer_library_load_trailers_by_remote_id($item->getId());
      $trailer = reset($trailers);
      $operations['view'] = [
        'title' => t('View'),
        'weight' => 5,
        'url' => $trailer->toUrl(),
      ];
      $operations['edit'] = [
        'title' => t('Edit'),
        'weight' => 10,
        'url' => $trailer->toUrl('edit-form'),
      ];
    }
    else {
      $operations['create'] = [
        'title' => 'Aus Bibliothek übernehmen',
        'weight' => 10,
        'url' => Url::fromRoute('entity.trailer.from_library', ['remote_id' => $item->getId()]),
      ];
    }
    return [
      '#type' => 'operations',
      '#links' => $operations,
    ];
  }

}
