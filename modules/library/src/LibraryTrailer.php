<?php

namespace Drupal\unsm_trailer_library;

use Drupal\physical\Length;
use Drupal\physical\Weight;

/**
 * Value object representing a remote trailer from library.
 */
final class LibraryTrailer extends LibraryTrailerBase {

  /**
   * The internal length.
   *
   * @var \Drupal\physical\Length
   */
  protected $internalLength;

  /**
   * The internal width.
   *
   * @var \Drupal\physical\Length
   */
  protected $internalWidth;

  /**
   * The internal height.
   *
   * @var \Drupal\physical\Length
   */
  protected $internalHeight;

  /**
   * The loading height.
   *
   * @var \Drupal\physical\Length
   */
  protected $loadingHeight;

  /**
   * The inner height.
   *
   * @var \Drupal\physical\Length
   */
  protected $innerHeight;

  /**
   * The load capacity.
   *
   * @var \Drupal\physical\Weight
   */
  protected $loadCapacity;

  /**
   * The total weight.
   *
   * @var \Drupal\physical\Weight
   */
  protected $totalWeight;

  /**
   * The description text.
   *
   * @var string
   */
  protected $description;

  /**
   * The tyres.
   *
   * @var string
   */
  protected $tyres;

  /**
   * The (remote) image url.
   *
   * @var string
   */
  protected $imageUrl;

  /**
   * Get the internal length.
   *
   * @return \Drupal\physical\Length|null
   *   The internal length.
   */
  public function getInternalLength() {
    return $this->internalLength;
  }

  /**
   * Get the internal width.
   *
   * @return \Drupal\physical\Length|null
   *   The internal width.
   */
  public function getInternalWidth() {
    return $this->internalWidth;
  }

  /**
   * Get the internal height.
   *
   * @return \Drupal\physical\Length|null
   *   The internal height.
   */
  public function getInternalHeight() {
    return $this->internalHeight;
  }

  /**
   * Get the internal dimensions as field value array.
   *
   * @return array
   *   The internal dimensions, as stored in the database.
   */
  public function getInternalDimensions() {
    $dimensions = [];
    if ($this->internalLength && $this->internalWidth && $this->internalHeight) {
      $dimensions['length'] = $this->internalLength->getNumber();
      $dimensions['width'] = $this->internalWidth->getNumber();
      $dimensions['height'] = $this->internalHeight->getNumber();
      $dimensions['unit'] = $this->internalLength->getUnit();
    }
    return $dimensions;
  }

  /**
   * Get the loading height.
   *
   * @return \Drupal\physical\Length|null
   *   The loading height.
   */
  public function getLoadingHeight() {
    return $this->loadingHeight;
  }

  /**
   * Get the inner height.
   *
   * @return \Drupal\physical\Length|null
   *   The inner height.
   */
  public function getInnerHeight() {
    return $this->innerHeight;
  }

  /**
   * Get the load capacity.
   *
   * @return \Drupal\physical\Weight|null
   *   The load capacity.
   */
  public function getLoadCapacity() {
    return $this->loadCapacity;
  }

  /**
   * Get the total weight.
   *
   * @return \Drupal\physical\Weight|null
   *   The total weight.
   */
  public function getTotalWeight() {
    return $this->totalWeight;
  }

  /**
   * Get the description text.
   *
   * @return string
   *   The description text.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Get the tyres.
   *
   * @return string
   *   The tyres.
   */
  public function getTyres() {
    return $this->tyres;
  }

  /**
   * Get the (remote) image url.
   *
   * @return string
   *   The (remote) image url.
   */
  public function getImageUrl() {
    return $this->imageUrl;
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $values) {
    $result = parent::fromArray($values);
    if (!empty($values['internal_length'])) {
      $result->internalLength = new Length($values['internal_length']['number'], $values['internal_length']['unit']);
    }
    if (!empty($values['internal_width'])) {
      $result->internalWidth = new Length($values['internal_width']['number'], $values['internal_width']['unit']);
    }
    if (!empty($values['internal_height'])) {
      $result->internalHeight = new Length($values['internal_height']['number'], $values['internal_height']['unit']);
    }
    if (!empty($values['loading_height'])) {
      $result->loadingHeight = new Length($values['loading_height']['number'], $values['loading_height']['unit']);
    }
    if (!empty($values['inner_height'])) {
      $result->innerHeight = new Length($values['inner_height']['number'], $values['inner_height']['unit']);
    }
    if (!empty($values['load_capacity'])) {
      $result->loadCapacity = new Weight($values['load_capacity']['number'], $values['load_capacity']['unit']);
    }
    if (!empty($values['total_weight'])) {
      $result->totalWeight = new Weight($values['total_weight']['number'], $values['total_weight']['unit']);
    }
    $result->description = $values['description'];
    $result->tyres = $values['tyres'];
    if (!empty($values['image'])) {
      $result->imageUrl = $values['image']['url'];
    }
    return $result;
  }

}
