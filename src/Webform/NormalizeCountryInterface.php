<?php

namespace Drupal\unsm_trailer\Webform;

/**
 * Provides country code mapping functionality for our webform handlers.
 */
interface NormalizeCountryInterface {

  /**
   * Normalizes the submitted country (name) value to a valid ISO code.
   *
   * @param string $country
   *   The submitted country name of the webform submission.
   * @param bool $force_valid_iso_code
   *   Whether or not returning a valid ISO code should be forced. Note that, if
   *   this option is set to TRUE, then invalid/unknown countries lead to an
   *   empty string result. Otherwise, the country input value will be returned.
   *   Defaults to FALSE.
   *
   * @return string
   *   The normalized country code. Note that it is not guaranteed that an ISO
   *   code is returned.
   */
  public function normalizeCountry($country, $force_valid_iso_code = FALSE);

}
