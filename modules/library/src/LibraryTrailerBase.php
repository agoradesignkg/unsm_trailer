<?php

namespace Drupal\unsm_trailer_library;

use Drupal\commerce_price\Price;

/**
 * Abstract base class for library trailer item value objects.
 */
abstract class LibraryTrailerBase {

  /**
   * The remote trailer ID.
   *
   * @var int
   */
  protected $id;

  /**
   * The trailer name.
   *
   * @var string
   */
  protected $title;

  /**
   * The category ID.
   *
   * @var int
   */
  protected $categoryId;

  /**
   * The category name.
   *
   * @var string
   */
  protected $categoryName;

  /**
   * The trailer bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The trailer bundle label.
   *
   * @var string
   */
  protected $bundleLabel;

  /**
   * The price.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected $price;

  /**
   * The published status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The created timestamp.
   *
   * @var int
   */
  protected $created;

  /**
   * The changed timestamp.
   *
   * @var int
   */
  protected $changed;

  /**
   * Factory method, instantiating a new TrailerLibraryListItem object from array.
   *
   * The array structure is based on the expectations of the structure returned
   * by UNSINN trailer library REST web service.
   *
   * @param array $values
   *   The values of a single result row.
   *
   * @return static
   *   A new TrailerLibraryListItem object, instantiated with the given values.
   */
  public static function fromArray(array $values) {
    $result = new static();
    $result->id = (int) $values['id'];
    $result->title = $values['title'];
    $result->categoryId = (int) $values['category_id'];
    $result->categoryName = $values['category'];
    $result->bundle = $values['bundle'];
    switch ($values['bundle']) {
      case 'remaining_stock':
        $result->bundleLabel = 'Aktionsanhänger';
        break;

      case 'secondhand':
        $result->bundleLabel = 'Gebrauchtanhänger';
        break;

      default:
        $result->bundleLabel = $values['bundle'];
    }
    if (!empty($values['price'])) {
      $result->price = new Price($values['price']['number'], $values['price']['currency_code']);
    }
    $result->status = (bool) $values['status'];
    $result->created = (int) $values['created'];
    $result->changed = (int) $values['changed'];

    return $result;
  }

  /**
   * Get the ID.
   *
   * @return int
   *   The ID.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Get the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Get the category ID.
   *
   * @return int
   *   The category ID.
   */
  public function getCategoryId() {
    return $this->categoryId;
  }

  /**
   * Get the category name.
   *
   * @return string
   *   The category name.
   */
  public function getCategoryName() {
    return $this->categoryName;
  }

  /**
   * Get the bundle ID.
   *
   * @return string
   *   The bundle ID.
   */
  public function getBundle() {
    return $this->bundle;
  }

  /**
   * Get the bundle label.
   *
   * @return string
   *   The bundle label.
   */
  public function getBundleLabel() {
    return $this->bundleLabel;
  }

  /**
   * Get the price.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The price.
   */
  public function getPrice() {
    return $this->price;
  }

  /**
   * Get whether the trailer is published.
   *
   * @return bool
   *   TRUE, if the trailer is published on unsinn.de, FALSE otherwise.
   */
  public function isPublished() {
    return $this->status;
  }

  /**
   * Get the created timestamp.
   *
   * @return int
   *   The created timestamp.
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * Get the changed timestamp.
   *
   * @return int
   *   The changed timestamp.
   */
  public function getChanged() {
    return $this->changed;
  }

  /**
   * Returns an array representation of the object, as used for serialization.
   *
   * @return string[]
   *   The array representation of the object, as used for serialization.
   */
  public function toArray() {
    return [
      'id' => $this->id,
      'title' => $this->title,
      'category_id' => $this->categoryId,
      'category' => $this->categoryName,
      'bundle' => $this->bundle,
      'price' => $this->price ? $this->price->toArray() : [],
      'status' => $this->status,
      'created' => $this->created,
      'changed' => $this->changed,
    ];
  }

}
