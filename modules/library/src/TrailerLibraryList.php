<?php

namespace Drupal\unsm_trailer_library;

/**
 * Value object representing the trailer library list.
 */
final class TrailerLibraryList {

  /**
   * @var \Drupal\unsm_trailer_library\TrailerLibraryListItem[]
   */
  protected $items;

  /**
   * Total number of rows.
   *
   * @var int
   */
  protected $totalRows;

  /**
   * Factory method, instantiating a new TrailerLibraryList object.
   *
   * The array structure (of the items) is based on the expectations of the
   * structure returned by UNSINN trailer library REST web service.
   *
   * @param array $items
   *   The JSON response, as returned from UNSINN trailer library web service.
   *
   * @return static
   *   A new FinderResult object, instantiated based on the given values.
   */
  public static function fromArray(array $items) {
    $result = new static();
    $result->items = [];

    foreach ($items as $item) {
      $result->items[] = TrailerLibraryListItem::fromArray($item);
    }
    $result->totalRows = count($result->items);

    return $result;
  }

  /**
   * Get the items.
   *
   * @return \Drupal\unsm_trailer_library\TrailerLibraryListItem[]
   *   The items.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Get the total number of rows.
   *
   * @return int
   *   The total number of rows.
   */
  public function getTotalRows() {
    return $this->totalRows;
  }

}
