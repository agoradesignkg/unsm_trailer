<?php

namespace Drupal\unsm_trailer_library\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\unsm_trailer_library\TrailerLibraryServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the confirmation form for cloning a library trailer.
 */
class ConfirmCloneFromLibraryForm extends ConfirmFormBase {

  /**
   * The remote trailer ID.
   *
   * @var int
   */
  protected $remoteId;

  /**
   * The trailer library service.
   *
   * @var \Drupal\unsm_trailer_library\TrailerLibraryServiceInterface
   */
  protected $trailerLibraryService;

  /**
   * Constructs a new ConfirmCloneFromLibraryForm object.
   *
   * @param \Drupal\unsm_trailer_library\TrailerLibraryServiceInterface $trailer_library_service
   *   The trailer library service.
   */
  public function __construct(TrailerLibraryServiceInterface $trailer_library_service) {
    $this->trailerLibraryService = $trailer_library_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unsm_trailer.trailer_library_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $remote_id = NULL) {
    $this->remoteId = (int) $remote_id;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to copy the trailer from UNSINN library into your website?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('unsm_trailer_library.trailer_library.overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_library_trailer_clone_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trailer = $this->trailerLibraryService->cloneFromLibrary($this->remoteId);
    if (empty($trailer)) {
      $this->messenger()->addError('Beim Erstellen des Anhängers ist ein Fehler aufgetreten!');
      $form_state->setRedirect('unsm_trailer_library.trailer_library.overview');
    }
    else {
      $this->messenger()->addStatus('Der Anhänger wurde erfolgreich gespeichert!');
      $form_state->setRedirectUrl($trailer->toUrl());
    }
  }

}
